core = 7.x
api = 2

; page_load_progress
projects[page_load_progress][type] = "module"
projects[page_load_progress][download][type] = "git"
projects[page_load_progress][download][url] = "https://git.uwaterloo.ca/drupal-org/page_load_progress.git"
projects[page_load_progress][download][tag] = "7.x-1.3+2-uw_wcms"
projects[page_load_progress][subdir] = ""

; uw_important_dates_remote_site
projects[uw_important_dates_remote_site][type] = "module"
projects[uw_important_dates_remote_site][download][type] = "git"
projects[uw_important_dates_remote_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_important_dates_remote_site.git"
projects[uw_important_dates_remote_site][download][tag] = "7.x-1.10"
projects[uw_important_dates_remote_site][subdir] = ""

; uw_cfg_calendar
projects[uw_cfg_calendar][type] = "module"
projects[uw_cfg_calendar][download][type] = "git"
projects[uw_cfg_calendar][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_calendar.git"
projects[uw_cfg_calendar][download][tag] = "7.x-1.2"
projects[uw_cfg_calendar][subdir] = ""

; calendar
projects[calendar][type] = "module"
projects[calendar][download][type] = "git"
projects[calendar][download][url] = "https://git.uwaterloo.ca/drupal-org/calendar.git"
projects[calendar][download][tag] = "7.x-3.5+1-dev-uw_wcms1"
projects[calendar][subdir] = ""

; uw_open_data_important_dates
projects[uw_open_data_important_dates][type] = "module"
projects[uw_open_data_important_dates][download][type] = "git"
projects[uw_open_data_important_dates][download][url] = "https://git.uwaterloo.ca/wcms/uw_open_data_important_dates.git"
projects[uw_open_data_important_dates][download][tag] = "7.x-1.4"

